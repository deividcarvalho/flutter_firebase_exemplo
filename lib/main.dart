import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Firebase Exemplo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  void enviarLivro(String titulo, String autor) {
    Firestore.instance
        .collection('livros')
        .document(titulo)
        .setData({'titulo': titulo, 'autor': autor});
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Center(
        child: new LivroLista(),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          setState(
            () {
              enviarLivro('As Crônicas de Gelo e Fogo', 'George R. R. Martin');
            },
          );
        },
        child: new Icon(Icons.add),
      ),
    );
  }
}

class LivroLista extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('livros').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) return new Text('Loading...');
        return new ListView(
          children: snapshot.data.documents.map((DocumentSnapshot document) {
            return new ListTile(
              title: new Text(document['titulo']),
              subtitle: new Text(document['autor']),
            );
          }).toList(),
        );
      },
    );
  }
}
